#!/bin/sh

name=$1
mkdir -p $(dirname $1)
curl -fsSL \
  -H 'Cache-Control: no-cache, no-store' \
  -H 'Pragma: no-cache' \
  https://codeberg.org/finwo/infrastructure/raw/branch/master/job/$name/job.hcl > $name.hcl

export DEFAULT_IFACE=$(ip route | grep default | tr ' ' '\n' | grep dev -A 1 | tail -1)
export DEFAULT_IP=$(ip route | grep default | tr ' ' '\n' | grep src -A 1 | tail -1)
export DATACENTER=$(curl -fsSL "http://metadata.google.internal/computeMetadata/v1/instance/attributes/datacenter" -H "Metadata-Flavor: Google")
export NM_AGENT=$(curl -fsSL "http://metadata.google.internal/computeMetadata/v1/instance/attributes/nm-agent" -H "Metadata-Flavor: Google")
export NFS_AGENT=$(curl -fsSL "http://metadata.google.internal/computeMetadata/v1/instance/attributes/nfs-agent" -H "Metadata-Flavor: Google")

# https://raw.githubusercontent.com/finwo/infrastructure/master/job/$name/job.hcl > $name.hcl
nomad job run $name.hcl && rm $name.hcl
